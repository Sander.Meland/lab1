package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yOrn = Arrays.asList("y", "n");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
        // start spill
        System.out.printf("Let's play round %d\n", (roundCounter));

        // spille
        String human = userChoice();
        String computer = computerChoice();
        String gameString = String.format("Human chose %s, computer chose %s.", human, computer);


        // sjekke hvem som vant og plusse på poeng

        whowon(human, computer, gameString);

        //spøre om ny runde

        String continuePlaying =continueGame();
        if (continuePlaying.equals("n")){
            break;
        }
        else{
            roundCounter++;
        }

        
        }
        // bye bye hvis ferdig
    System.out.println("Bye bye :)");
    }

       
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public String userChoice(){
        while (true){
            String human = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validation(human, rpsChoices)){
                return human;
            }
            else{
                System.out.printf("I don't understand %s. Could you try again?\n", human);

            }    
                
        }
    }

     public boolean validation(String input, List<String> list){
         return (list.contains(input));        
    }

    //public boolean validation2(String readInput){
      //  return (yOrn.contains(readInput));
    //}

 
    
    Random rand = new Random();
    public String computerChoice() {
        int rnd = rand.nextInt(3);
        String comp = rpsChoices.get(rnd);
        return comp;
    

    }

    void whowon(String p1, String p2 ,String string){

        if (isWinner(p1, p2)){
            System.out.println(string + " Human wins.");
            humanScore ++;
        }
        else if (isWinner(p2, p1)){
            System.out.println(string + " Computer wins.");
            computerScore ++;
        }
        else{
            System.out.println(string + " It's a tie");
        }
        System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
    }


    public boolean isWinner(String p1, String p2){
        if (p1.equals("paper")){
            return p2.equals("rock");
        }
        else if (p1.equals("rock")){
            return p2.equals("scissors");
        }
        else {
            return p2.equals("paper");
        }
    }
       
    public String continueGame(){
        while (true){
            String input = readInput("Do you wish to continue playing? (y/n)?");
            if (validation(input, yOrn)){
                return input;
            }
            else{
                System.out.printf("I don't understand %s. Could you try again?\n", input);
            }

        }
    
    
        

    }

    
    


    
    
}
